﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Shop.Dto;
using Shop.Enums;
using Shop.Models;

namespace Shop
{
	public class ProductService
	{
		private readonly ShopContext _dbContext;

		private IList<Product> Products => _dbContext.Products.Include(p => p.Category).ToList();

		public ProductService(ShopContext dbContext)
		{
			_dbContext = dbContext;
		}

		/// <summary>
		/// Сортирует товары по цене и возвращает отсортированный список
		/// </summary>
		/// <param name="sortOrder">Порядок сортировки</param>
		public IEnumerable<Product> SortByPrice(SortOrder sortOrder)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Возвращает товары, название которых начинается на <see cref="name"/>
		/// </summary>
		/// <param name="name">Фильтр - строка, с которой начинается название товара</param>
		public IEnumerable<Product> FilterByNameStart(string name)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Группирует товары по производителю
		/// </summary>
		public IDictionary<string, List<Product>> GroupByVendor()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Возвращает список самых дорогих товаров (самые дорогие - товары с наибольшей ценой среди всех товаров)
		/// </summary>
		public IEnumerable<Product> GetTheMostExpensiveProducts()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Возвращает список самых дешевых товаров (самые дешевые - товары с наименьшей ценой среди всех товаров)
		/// </summary>
		public IEnumerable<Product> GetTheCheapestProducts()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Возвращает среднюю цену среди всех товаров
		/// </summary>
		public decimal GetAverageProductPrice()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Возвращает среднюю цену товаров в указанной категории
		/// </summary>
		/// <param name="categoryId">Идентификатор категории</param>
		public decimal GetAverageProductPriceInCategory(int categoryId)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Возвращает список продуктов с актуальной ценой (после применения скидки)
		/// </summary>
		public IDictionary<Product, decimal> GetProductsWithActualPrice()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Возвращает список продуктов, сгруппированный по производителю, а внутри - по названию категории.
		/// Продукты внутри последней группы отсортированы в порядке убывания цены
		/// </summary>
		public IList<VendorProductsDto> GetGroupedByVendorAndCategoryProducts()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Обновляет скидку на товары, которые остались на складе в количестве 1 шт,
		/// и возвращает список обновленных товаров
		/// </summary>
		/// <param name="newDiscount">Новый процент скидки</param>
		public IEnumerable<Product> UpdateDiscountIfUnitsInStockEquals1AndGetUpdatedProducts(int newDiscount)
		{
			throw new NotImplementedException();
		}
	}
}
